# SPIE 2022 Supplementary Material

## Direct Optimization of $`\lambda`$ for HDR Content Adaptive Transcoding in AV1

Author(s): 

Vibhoothi, François Pitié, Angeliki Katsenou, Daniel Joseph Ringis, Trinity
College Dublin (Ireland); Yeping Su, Neil Birkbeck, Jessie Lin, Balu Adsumilli,
Google (United States); Anil Kokaram, Trinity College Dublin (Ireland)

Contact: vibhoothi@tcd.ie, pitief@tcd.ie, anil.kokaram@tcd.ie

Conference Homepage: [SPIE Applications of Digital Image Processing XLV
](https://spie.org/optics-photonics/presentation/Direct-Optimization-of-%ce%bb-for-HDR-Content-Adaptive-Transcoding-in/12226-6)


### Table of Contents 
[[_TOC_]]


### Abstract 

Since the adoption of VP9 by Netflix in 2016, royalty-free coding standards
continued to gain prominence through the activities of the AOMedia consortium.
AV1, the latest open source standard, is now widely supported. In
the early years after standardisation, HDR video tends to be under served in
open source encoders for a variety of reasons including the relatively small
amount of true HDR content being broadcast and the challenges in RD optimisation
with that material. AV1 codec optimisation has been ongoing since 2020 including
consideration of the computational load. In this paper, we
explore the idea of direct optimisation of the Lagrangian $`\lambda`$ parameter
used in the rate control of the encoders to estimate the optimal Rate-Distortion
trade-off achievable for a High Dynamic Range signalled video clip. We show that
by adjusting the Lagrange multiplier in the RD optimisation process on a
frame-hierarchy basis, we are able to increase the Bjontegaard
difference rate gains by more than 3.98x on average without
visually affecting the quality.


### Dataset

Clip Information: 

<Details>

| #   | Name                                              | Video set       | Shot group          | Resolution | Framerate |
| --- | ------------------------------------------------- | --------------- | ------------------- | ---------- | --------- |
| 1   | MeridianRoad_3840x2160_5994_hdr10.y4m             | aom-ctc         | Netflix Meridian    | 3840x2160  | 59.94     |
| 2   | NocturneDance_3840x2160_60fps_hdr10.y4m           | aom-ctc         | Netflix Nocturne    | 3840x2160  | 60        |
| 3   | NocturneRoom_3840x2160_60fps_hdr10.y4m            | aom-ctc         | Netflix Nocturne    | 3840x2160  | 60        |
| 4   | SparksWelding_4096x2160_5994_hdr10.y4m            | aom-ctc         | Netflix Sparks      | 4096x2160  | 59.94     |
| 5   | aom_cosmos_3840x2160_24_hdr10_1573-1749.y4m       | aom-candidates  | Netflix Cosmos      | 3840x2160  | 24        |
| 6   | aom_cosmos_3840x2160_24_hdr10_8686-8826.y4m       | aom-candidates  | Netflix Cosmos      | 3840x2160  | 24        |
| 7   | aom_cosmos_3840x2160_24_hdr10_9561-9789.y4m       | aom-candidates  | Netflix Cosmos      | 3840x2160  | 24        |
| 8   | aom_cosmos_3840x2160_24_hdr10_11589-11752.y4m     | aom-candidates  | Netflix Cosmos      | 3840x2160  | 24        |
| 9   | aom_cosmos_3840x2160_24_hdr10_12149-12330.y4m     | aom-candidates  | Netflix Cosmos      | 3840x2160  | 24        |
| 10  | aom_cosmos_3840x2160_24_hdr10_12916-13078.y4m     | aom-candidates  | Netflix Cosmos      | 3840x2160  | 24        |
| 11  | aom_cosmos_3840x2160_24_hdr10_13446-13649.y4m     | aom-candidates  | Netflix Cosmos      | 3840x2160  | 24        |
| 12  | aom_meridian_3840x2160_5994_hdr10_1782-2163.y4m   | aom-candidates  | Netflix Meridian    | 3840x2160  | 59.94     |
| 13  | aom_meridian_3840x2160_5994_hdr10_12264-12745.y4m | aom-candidates  | Netflix Meridian    | 3840x2160  | 59.94     |
| 14  | aom_meridian_3840x2160_5994_hdr10_15932-16309.y4m | aom-candidates  | Netflix Meridian    | 3840x2160  | 59.94     |
| 15  | aom_meridian_3840x2160_5994_hdr10_20988-21412.y4m | aom-candidates  | Netflix Meridian    | 3840x2160  | 59.94     |
| 16  | aom_meridian_3840x2160_5994_hdr10_22412-22738.y4m | aom-candidates  | Netflix Meridian    | 3840x2160  | 59.94     |
| 17  | aom_meridian_3840x2160_5994_hdr10_24058-24550.y4m | aom-candidates  | Netflix Meridian    | 3840x2160  | 59.94     |
| 18  | aom_nocturne_3840x2160_60_hdr10_2370-2539.y4m     | aom-candidates  | Netflix Nocturne    | 3840x2160  | 60        |
| 19  | aom_nocturne_3840x2160_60_hdr10_8540-9009.y4m     | aom-candidates  | Netflix Nocturne    | 3840x2160  | 60        |
| 20  | aom_nocturne_3840x2160_60_hdr10_9010-9349.y4m     | aom-candidates  | Netflix Nocturne    | 3840x2160  | 60        |
| 21  | aom_nocturne_3840x2160_60_hdr10_17140-17709.y4m   | aom-candidates  | Netflix Nocturne    | 3840x2160  | 60        |
| 22  | aom_nocturne_3840x2160_60_hdr10_27740-28109.y4m   | aom-candidates  | Netflix Nocturne    | 3840x2160  | 60        |
| 23  | aom_nocturne_3840x2160_60_hdr10_32660-32799.y4m   | aom-candidates  | Netflix Nocturne    | 3840x2160  | 60        |
| 24  | aom_sol_levante_3840x2160_24_hdr10_289-453.y4m    | aom-candidates  | Netflix Sol Levante | 3840x2160  | 24        |
| 25  | aom_sol_levante_3840x2160_24_hdr10_519-649.y4m    | aom-candidates  | Netflix Sol Levante | 3840x2160  | 24        |
| 26  | aom_sol_levante_3840x2160_24_hdr10_2268-2412.y4m  | aom-candidates  | Netflix Sol Levante | 3840x2160  | 24        |
| 27  | aom_sol_levante_3840x2160_24_hdr10_3282-3874.y4m  | aom-candidates  | Netflix Sol Levante | 3840x2160  | 24        |
| 28  | aom_sol_levante_3840x2160_24_hdr10_4123-4545.y4m  | aom-candidates  | Netflix Sol Levante | 3840x2160  | 24        |
| 29  | aom_sparks_4096x2160_5994_hdr10_230-360.y4m       | aom-candidates  | Netflix Sparks      | 4096x2160  | 59.94     |
| 30  | aom_sparks_4096x2160_5994_hdr10_350-480.y4m       | aom-candidates  | Netflix Sparks      | 4096x2160  | 59.94     |
| 31  | aom_sparks_4096x2160_5994_hdr10_2024-2455.y4m     | aom-candidates  | Netflix Sparks      | 4096x2160  | 59.94     |
| 32  | aom_sparks_4096x2160_5994_hdr10_5363-5763.y4m     | aom-candidates  | Netflix Sparks      | 4096x2160  | 59.94     |
| 33  | aom_sparks_4096x2160_5994_hdr10_6026-6502.y4m     | aom-candidates  | Netflix Sparks      | 4096x2160  | 59.94     |
| 34  | aom_sparks_4096x2160_5994_hdr10_8396-8941.y4m     | aom-candidates  | Netflix Sparks      | 4096x2160  | 59.94     |
| 35  | aom_sparks_4096x2160_5994_hdr10_9774-10071.y4m    | aom-candidates  | Netflix Sparks      | 4096x2160  | 59.94     |
| 36  | aom_sparks_4096x2160_5994_hdr10_11198-11570.y4m   | aom-candidates  | Netflix Sparks      | 4096x2160  | 59.94     |
| 37  | svt_forest_lake_3840x2160_50_10bit_420.y4m        | svt-opencontent | SVT                 | 3840x2160  | 50        |
| 38  | svt_midnight_sun_3840x2160_50_10bit_420.y4m       | svt-opencontent | SVT                 | 3840x2160  | 50        |
| 39  | svt_smithy_3840x2160_50_10bit_420.y4m             | svt-opencontent | SVT                 | 3840x2160  | 50        |
| 40  | svt_smoke_sauna_3840x2160_50_10bit_420.y4m        | svt-opencontent | SVT                 | 3840x2160  | 50        |
| 41  | svt_waterfall_3840x2160_50_10bit_420.y4m          | svt-opencontent | SVT                 | 3840x2160  | 50        |
| 42  | svt_water_flyover_3840x2160_50_10bit_420.y4m      | svt-opencontent | SVT                 | 3840x2160  | 50        |
| 43  | wipe_3840x2160_5994_10bit_420_881-1011.y4m        | cables-4k       | Cables 4K           | 3840x2160  | 59.94     |
| 44  | wipe_3840x2160_5994_10bit_420_1747-1877.y4m       | cables-4k       | Cables 4K           | 3840x2160  | 59.94     |
| 45  | wipe_3840x2160_5994_10bit_420_2530-2660.y4m       | cables-4k       | Cables 4K           | 3840x2160  | 59.94     |
| 46  | wipe_3840x2160_5994_10bit_420_3857-3987.y4m       | cables-4k       | Cables 4K           | 3840x2160  | 59.94     |
| 47  | wipe_3840x2160_5994_10bit_420_4653-4783.y4m       | cables-4k       | Cables 4K           | 3840x2160  | 59.94     |
| 48  | wipe_3840x2160_5994_10bit_420_6309-6439.y4m       | cables-4k       | Cables 4K           | 3840x2160  | 59.94     |
| 49  | wipe_3840x2160_5994_10bit_420_7250-7380.y4m       | cables-4k       | Cables 4K           | 3840x2160  | 59.94     |
| 50  | wipe_3840x2160_5994_10bit_420_8750-8880.y4m       | cables-4k       | Cables 4K           | 3840x2160  | 59.94     |

</Details>

We could actually group these 50 sequences into 7 different content-group,
| Shot Group                                                                                                         | Description                                                  |
| ------------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------------ |
| Cosmos<sup>[1](https://opencontent.netflix.com/)</sup>                                                             | Vibrant animated sequence, high temporal complexity, 24fps   |
| Meridian<sup>[1](https://opencontent.netflix.com/),[2](https://aomedia.org/docs/CWG-B075o_AV2_CTC_v2.pdf)</sup>    | Natural sequence, high spatial complexity, 59.94fps          |
| Nocturne<sup>[1](https://opencontent.netflix.com/),[2](https://aomedia.org/docs/CWG-B075o_AV2_CTC_v2.pdf)</sup>    | Natural sequence, medium spatial complexity, 60fps           |
| Sol Levante<sup>[1](https://opencontent.netflix.com/),[2](https://aomedia.org/docs/CWG-B075o_AV2_CTC_v2.pdf)</sup> | Animated sequence, medium-high temporal complexity, 24fps    |
| Sparks<sup>[1](https://opencontent.netflix.com/),[2](https://aomedia.org/docs/CWG-B075o_AV2_CTC_v2.pdf)</sup>      | Sequence with medium motion and wide dynamic range, 59.94fps |
| SVT<sup>[3](https://www.svt.se/open/en/content/)</sup>                                                             | Very high natural complexity sequences, 50fps                |
| Cables 4K<sup>[4](https://www.cablelabs.com/4k)</sup>                                                              | Outdoor sequences with moderate complexity, 59.94fps         |

Sample frames are below,

![Frame sequences](data/img/seq_grid.png)


The source for these clips are:

i) [Netflix Open Content](https://opencontent.netflix.com/),

ii) [Cables 4K](https://www.cablelabs.com/4k)

iii) [SVT Open Content](https://www.svt.se/open/en/content/)

These sequences have been adopted by various standards bodies, so for a subset
of the sequences the processed HDR variant from the raw video/image sequence is
available. Below list shows the information for the same, 

i) Netflix Sequences:
[AOM-CTC](https://media.xiph.org/video/aomctc/test_set/hdr1_4k/),
[AOM-Candidates](https://media.xiph.org/video/av2/),
[Netflix](http://download.opencontent.netflix.com.s3.amazonaws.com/index.html?prefix=aom_test_materials/HDR/).
From the JPEG200 (j2k) source, we converted to tiff sequences first and later
converted them YUV videos with help of [HDR
Tools](https://gitlab.com/standards/HDRTools) with a custom configuration and
last to Y4M. The config file which we used for this conversion is
[HDRConvertP3D65TiffToYCbCr420.cfg](data/config/HDRConvertP3D65TiffToYCbCr420.cfg).

ii) Cables 4K: These sequences are manually trimmed and reformatted with help of [HDR
Tools](https://gitlab.com/standards/HDRTools). Some of the sequneces are used in
[3GPP Standardization](https://github.com/haudiobe/5GVideo) where Clip is named
[Life
Untouched](https://dash-large-files.akamaized.net/WAVE/3GPP/5GVideo/ReferenceSequences/Life-Untouched/).
The config file which we used for this conversion is
[HDRConvertP3D65TiffToYCbCr420.cfg](data/config/HDRConvertP3D65TiffToYCbCr420.cfg).

iii) SVT: These are very new sequences released by Swedish Television Group,
these are open-sourced, these were released as YUV444P12 MOV files, so we
manually converted to YUV444p12 yuv and then yuv420p10 via HDRTools, and later
to y4m. We have mirrored the sequences from FTP location to
[media.xiph.org/svt/2022](https://media.xiph.org/svt/2022/). The config file
which we used was
[HDRConvertYUV444P12toYUV420P10.cfg](data/config/HDRConvertYUV444P12toYUV420P10.cfg).

### Spatial-Temporal Information for the dataset
In order to get an idea of the video parameter space coverage with the selected
dataset, we computed the low-level features as recommended in
[ITU-P.910](https://www.itu.int/rec/T-REC-P.910). Figure below shows the variation of
Spatial and Temporal Information (SI and TI) and Dynamic Range (DR) grouped by
shots from our corpus. SI is defined as the highest value of the standard
deviation of the Sobel filtered frame, while TI is computed based on motion
difference between adjacent frames and maximum value of the standard deviation
is used. The DR of a sequence is computed as per [Barman et.
al](https://ieeexplore.ieee.org/document/9422736)
([GitHub](https://github.com/NabajeetBarman/CalculateDynamicRange)), where
values of luminance plane by removing the top 1\% and bottom 1\%  is computed,
and the average value of lowest to highest luminance is expressed in $log_2$ is
used. Naturally, when the sequences is having spatially complex scenes we will
be having higher SI, and when there is more motion in the adjacent frames, we
will be having higher TI. Generally,higher SI and TI values denote a more
complex sequence to encode. The SI values for our corpus ranges from 153-913
with a median of 424, while TI values range from 5.8 to 164 with a median of
45.9. The dynamic range varies from 0.45 to 3.49, with a median of 2.24. Thus,
the corpus covers wide range of complexities.

| ![image](data/img/si-ti-hdr.png)     | ![image](data/img/si-dr-hdr.png)     |
| ------------------------------------ | ------------------------------------ |
| <center>Spatial Information</center> | <center>Dynamic Information</center> |

### Encoder Configuration

libaom-av1-3.2.0, [287164d](https://aomedia.googlesource.com/aom/+/287164d)
``` sh
$AOMEMC --cpu-used=0 --passes=1 --lag-in-frames=19 --auto-alt-ref=1 --min-gf-interval=16 --max-gf-interval=16
--gf-min-pyr-height=4 --gf-max-pyr-height=4 --limit=130 --kf-min-dist=65 --kf-max-dist=65 --use-fixed-qp-offsets=1
--deltaq-mode=0 --enable-tpl-model=0 --end-usage=q --cq-level=$Q --enable-keyframe-filtering=0 --threads=1
--test-decode=fatal -o output.ivf --color-primaries=bt2020 --transfer-characteristics=smpte2084
--matrix-coefficients=bt2020ncl --chroma-sample-position=colocated $OPTIONS $INPUT.Y4M
```
> GOP-Size is fixed at 65, Sub-GOP is 16, Temporal-layers is 4, Closed-GOP
GOP-Size is set in AV1 with help of `kf-min-dist` and `kf-max-dist`. Sub-GOP
size is set in AV1 using `min-gf-interval` and `max-gf-interval`. The temporal
layers is defined using `gf-min-pyr-height` and `gf-max-pyr-height`. To have a
hierarchical reference structure, the number of temporal layers should be set to
$log2(SubGopsize)$. Open-GOP and Closed GOP is controlled with help of `
--enable-fwd-kf` inside AV1. Color Primaries, Transfer Charecteristics, Matrix
Coffecients are also specified which is required HDR Metadata.

### Quantization Parameters(QP)

| SL-No | libaom-av1 |
| ----- | ---------- |
| 1     | 27         |
| 2     | 39         |
| 3     | 49         |
| 4     | 59         |
| 5     | 63         |

The QP points were chosen based on the [AOM-Common Testing Configuration(CTC.)](https://aomedia.org/docs/CWG-B075o_AV2_CTC_v2.pdf).



### *Encoder Modifications*
In, libaom-AV1, the Lagrangian $`\lambda`$ adjustments are not exposed by default. We have
modifed the encoder to support feeding the $`\lambda`$ values externally, and
inside the encoder, we adjust the $`\lambda`$ values based on the given encoding
conditions and frame-types.

### *Optimiser Setup*
For this experiment, we are relying on Brent's optimiser which is implemented
inside Scipy's Optmise
module's([scipy.optimize.fminbound](https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.fminbound.html)),
where we provided a high upper and lower bound
of the possible lambda adjustments. For multivariable search options, we are using Modified
Powell method which is implemented in Scipy's optimise module
[scipy.optimise.minimise](https://docs.scipy.org/doc/scipy/reference/optimize.minimize-powell.html#optimize-minimize-powell).

The scipy powell had couple of shortcomings or bugs like [bounds being
not respected](https://github.com/scipy/scipy/issues/15342), or [not returning
the lowest energy being visited](https://github.com/scipy/scipy/issues/15089) in
the upstream, so for our
experiment we deployed workaround for them to our optimiser framework to have a
deterministic behaviour of the optimier.

### Data Collection

The per clip data-point for each QP is collected with the help of
[libvmaf](https://github.com/Netflix/vmaf/tree/master/libvmaf) module of
[VMAF](https://github.com/Netflix/vmaf/),
Table below shows the metrics being collected for each runs, 

| SL No | Metrics Name | SL No    | Metrics Name  |
| ----- | ------------ | -------- | ------------- |
| 1     | PSNR Y       | 9        | PSNR-HVS Cb   |
| 2     | PSNR Cb      | 10       | PSNR-HVS Cr   |
| 3     | PSNR Cr      | 11       | Encoding Time |
| 4     | PSNR-HVS     | 12       | Decoding Time |
| 5     | CIEDE2000    | 13       | VMAF          |
| 6     | SSIM         | 14       | VMAF-NEG      |
| 7     | MS-SSIM      | 15       | APSNR Y       |
| 8     | PSNR-HVS Y   | 16       | APSNR Cb      |
|       | 17           | APSNR Cr |               |



### Experimental Results

Inside [data](data/), you can find per run type mode results, the files are
inside [csv](data/csv) folder.
The genral format of each file is `spie-hdr-4k-$SET-$MODE-proxy_opt.csv`, where 

`$SET` denotes the video set being used, there are 4 possible values, `aom` for
the AOM-Candidates set (33 videos), `wipe` for Cables-4K set (8), `svt` for the
SVT video set (6), `av2` for the AOM-CTC video set.

`$MODE` indicates the tuning mode deployed inside the codec to get the results.
It can be having 5 values, `t1`, `t2`, `t3`, `t5`, `powell-t5`.

The results of the Optimiser is found inside the [json](data/json) folder. The
general schema of the JSON data is shown below:

<details>

  ```json
  {
    "run_info": {
        "set"  :  "$RUN_NAME",
        "run_id"  :  "$RUN_ID",
        "extra_options"  :  "",
        "qualities"  :  [5],
        "cost_method"  :  "$COST_METHOD",
		    "optimiser_mode"  :  "$OPT_MODE",
		    "lambda_opt"  :  true,
        "tune_type"   : "$TUNE_VAL",
        "optimiser_method": "$OPT_METHOD",
        "opt_space" : "top",
        "slots"   : "$VAL",
        "init_range": [2],
		    "metrics"  :  "$METRICS"
        },
	"clip_name": {
	    "$OPT_METHOD_data"  : [4],
        "total_encodes"  :	$number,
	    "bdrate_data"  :  {
                          "$k_value" : { "$bdrate_values_metrics" }
                          },
	    "cost_pair"  :  {
                          "$k_value" : "$bdrate"
                        },
      "proxy_result": {
                          "final_k"    : "$k_value",
                          "bdrate_data": { "$bdrate_value_metrics" },
                          "cost_pair"  : { "k_value": "$bdrate" }
                      }
    }
  }
  ```

</details>

### RD-Curves

[TO BE FILLED]

### Summary Results

[TO BE FILLED]
### Analysis

[TO BE FILLED]
## Acknowledgements

This Work is part of Video Intelligence Search Platform(VISP) Project, which is
funded by Enterprise Ireland under Disruptive Technology Innovation Fund
(DTIF.), Grant No, DT-2019-0068 and Adapt Research Center for funding the
research work.

We would also like to thank Sigmedia.tv members, and people from
Open-source community and AOMedia members for giving feedbacks on
the implementation overtime.

